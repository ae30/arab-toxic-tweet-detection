## Partie de l’état d’art

### C’est quoi le Deep learning?

Le deep learning ou apprentissage profond est un type d'intelligence artificielle dérivé du ma-

chine learning (apprentissage automatique) où la machine est capable d'apprendre par elle-

même, contrairement à la programmation où elle se contente d'exécuter à la lettre des règles

prédéterminées.

### Fonctionnement du deep Learning

Le deep Learning s'appuie sur un réseau de neurones artificiels s'inspirant du cerveau humain.

Ce réseau est composé de dizaines voire de centaines de « couches » de neurones, chacune re-

cevant et interprétant les informations de la couche précédente. Le système apprendra par

exemple à reconnaître les lettres avant de s'attaquer aux mots dans un texte, ou détermine s'il y

a un visage sur une photo avant de découvrir de quelle personne il s'agit.

```
Figure 1 : Deep learning
```

### Les réseaux de neurones récurrents et LSTM

Les réseaux neuronaux récurrents (RNN) sont un type puissant et robuste de réseaux neuronaux

et appartiennent aux algorithmes les plus prometteurs du moment car ils sont les seuls à avoir

une mémoire interne.

Les RNN sont relativement anciens, comme beaucoup d'autres algorithmes d'apprentissage en

profondeur. Ils ont été initialement créés dans les années 1980, mais ne peuvent montrer leur

potentiel réel que depuis quelques années, en raison de l'augmentation de la puissance de calcul

disponible, des quantités massives de données que nous avons aujourd'hui et de l'invention de

LSTM dans les années 1990.

En raison de leur mémoire interne, les RNN sont capables de se souvenir de choses importantes

concernant les données qu'ils ont reçues, ce qui leur permet d'être très précis dans la prédiction

de ce qui va suivre.

C'est la raison pour laquelle ils sont l'algorithme préféré pour les données séquentielles comme

les séries temporelles, la parole, le texte, les données financières, audio, vidéo, météo et bien

plus parce qu'ils peuvent former une compréhension beaucoup plus profonde d'une séquence et

de son contexte.

### Comment ils travaillent?

Nous allons d'abord discuter de certains faits importants sur les réseaux neuronaux Feed

Forward « normaux », que vous devez savoir, pour comprendre correctement les réseaux neu-

ronaux récurrents.

Mais il est également important que vous compreniez ce que sont les données séquentielles. Il

s'agit essentiellement de données ordonnées, où les choses liées se suivent. Des exemples sont

des données financières ou la séquence d'ADN. Le type le plus courant de données séquentielles


IA & DL ENSA Khouribga

7

est peut-être les données de séries chronologiques, qui sont juste une série de points de données

répertoriés dans l'ordre chronologique.

## Figure 2: Architecture d'un RN

RNN et Feed-Forward Neural Networks sont tous deux nommés d'après la façon dont ils cana-

lisent l'information.

Dans un réseau de neurones Feed-Forward, l'information ne se déplace que dans une direction,

de la couche d'entrée, à travers les couches cachées, à la couche de sortie. L'information se

déplace directement à travers le réseau. Pour cette raison, l'information ne touche jamais un

nœud deux fois.

Feed-Forward Neural Networks, n'ont aucune mémoire de l'entrée qu'ils ont reçu précédemment

et sont donc mauvais dans la prédiction de ce qui va suivre. Parce qu'un réseau feedforward ne

considère que l'entrée actuelle, il n'a aucune notion d'ordre dans le temps. Ils ne peuvent tout

simplement pas se souvenir de ce qui s'est passé dans le passé, sauf de leur entraînement.

### Réseaux neuronaux récurrents................................................................................................

Dans un RNN, l'information parcourt une boucle. Quand il prend une décision, il prend en

compte l'entrée courante et aussi ce qu'il a appris des entrées qu'il a reçues précédemment.


Les deux images ci-dessous illustrent la différence dans le flux d'informations entre un RNN et

un réseau neuronal Feed-Forward.

## Figure 3: RNN et FFNN

Un RNN habituel a une mémoire à court terme. En combinaison avec un LSTM, ils ont aussi

une mémoire à long terme, mais nous en discuterons plus loin.

Un autre bon moyen d'illustrer le concept de la mémoire d'un RNN est de l'expliquer par un

exemple:

Imaginez que vous ayez un réseau de neurones feed-forward normal et que vous lui donniez le

mot "neurone" comme une entrée et qu'il traite le mot caractère par caractère. Au moment où il

atteint le caractère "r", il a déjà oublié "n", "e" et "u", ce qui rend presque impossible à ce type

de réseau de neurones de prédire quel personnage viendrait ensuite.

Un réseau neuronal récurrent est capable de se souvenir exactement de cela, à cause de sa mé-

moire interne. Il produit une sortie, copie cette sortie et la boucle dans le réseau.

Les réseaux neuronaux récurrents ajoutent le passé immédiat au présent.

Par conséquent, un réseau neuronal récurrent a deux entrées, le présent et le passé récent. Ceci

est important car la séquence de données contient des informations cruciales sur ce qui va


IA & DL ENSA Khouribga

9

suivre, ce qui explique pourquoi un RNN peut faire des choses que d'autres algorithmes ne

peuvent pas faire.

Un réseau neuronal Feed-Forward attribue, comme tous les autres algorithmes Deep Learning,

une matrice de pondération à ses entrées, puis produit la sortie. Notez que les RNN appliquent

des poids à l'entrée actuelle et à l'entrée précédente. En outre, ils ajustent aussi leurs poids pour

la descente en gradient et la propagation inverse à travers le temps, dont nous parlerons dans la

prochaine section ci-dessous.

Notez également que, bien que les réseaux neuronaux Feed-Forward fassent correspondre une

entrée à une sortie, les RNN peuvent correspondre à un à plusieurs, plusieurs à plusieurs (tra-

duction) et plusieurs à un (classification d'une voix).

## Figure 4: différents modèle de RNN

### Backpropagation à travers le temps

Pour comprendre le concept de rétropropagation à travers le temps, vous devez d'abord com-

prendre les concepts de propagation avant et arrière. Je ne vais pas entrer dans les détails ici

parce que ce serait loin de la limite de ce billet de blog, donc je vais essayer de vous donner une

définition de ces concepts qui est aussi simple que possible, mais vous permet de comprendre

le concept global de rétropropagation À travers le temps.


Dans les réseaux de neurones, vous faites essentiellement la propagation vers l'avant pour ob-

tenir la sortie de votre modèle et vérifier si cette sortie est correcte ou incorrecte, pour obtenir

l'erreur.

Maintenant vous faites Backward-Propagation, qui n'est rien d'autre que de revenir en arrière à

travers votre réseau de neurones pour trouver les dérivées partielles de l'erreur par rapport aux

poids, ce qui vous permet de soustraire cette valeur des poids.

Ces dérivées sont ensuite utilisées par Gradient Descent, un algorithme utilisé pour minimiser

itérativement une fonction donnée. Ensuite, il ajuste les poids vers le haut ou vers le bas, en

fonction de ce qui diminue l'erreur. C'est exactement la façon dont un réseau de neurones ap-

prend pendant le processus de formation.

Donc, avec Backpropagation, vous essayez essentiellement de modifier les poids de votre mo-

dèle, pendant l'entraînement.

L'image ci-dessous illustre parfaitement le concept de la propagation vers l'avant et de la pro-

pagation vers l'arrière à l'exemple d'un réseau de neurones Feed Forward:

## Figure 5: Forward propagation et backward propagation


IA & DL ENSA Khouribga

11

Backpropagation à travers le temps (BPTT) est fondamentalement juste un mot à la mode pour

faire backpropagation sur un réseau neuronal récurrent déroulé. Unrolling est un outil de visua-

lisation et de conceptualisation qui vous aide à comprendre ce qui se passe dans le réseau. La

plupart du temps, lorsque vous implémentez un réseau neuronal récurrent dans les cadres de

programmation communs, ils s'occupent automatiquement de la rétropropagation, mais vous

devez comprendre comment cela fonctionne, ce qui vous permet de résoudre les problèmes qui

surviennent au cours du processus de développement.

Vous pouvez voir un RNN comme une séquence de réseaux neuronaux que vous entraînez l'un

après l'autre avec rétropropagation.

L'image ci-dessous illustre un RNN déroulé. Sur la gauche, vous pouvez voir le RNN, qui est

déroulé après le signe égal. Notez qu'il n'y a pas de cycle après le signe égal puisque les diffé-

rents pas de temps sont visualisés et que l'information est passée d'une étape à l'autre. Cette

illustration montre également pourquoi un RNN peut être vu comme une séquence de réseaux

neuronaux.

## Figure 6 : RNN

Si vous faites Backpropagation Through Time, il est nécessaire de faire la conceptualisation du

déroulement, car l'erreur d'un pas de temps donné dépend du pas de temps précédent.

Dans BPTT, l'erreur est rétropropagée du dernier au premier pas de temps, tout en déroulant

tous les pas de temps. Cela permet de calculer l'erreur pour chaque pas de temps, ce qui permet


de mettre à jour les poids. Notez que BPTT peut être coûteux en calcul lorsque vous avez un

nombre élevé de timesteps.

### Mémoire à long terme à court terme LSTM

Les réseaux de longue mémoire à court terme (LSTM) sont une extension pour les réseaux

neuronaux récurrents, qui étend leur mémoire. Par conséquent, il est bien adapté pour apprendre

des expériences importantes qui ont des retards très longs entre les deux.

Les unités d'un LSTM sont utilisées comme unités de construction pour les couches d'un RNN,

qui est alors souvent appelé un réseau LSTM.

Les LSTM permettent aux RNN de se souvenir de leurs intrants sur une longue période de

temps. C'est parce que les LSTM contiennent leurs informations dans une mémoire, ce qui res-

semble beaucoup à la mémoire d'un ordinateur parce que le LSTM peut lire, écrire et supprimer

des informations de sa mémoire.

Cette mémoire peut être vue comme une cellule gated, où gated signifie que la cellule décide

de stocker ou de supprimer des informations (par exemple si elle ouvre les portes ou non), en

fonction de l'importance qu'elle attribue à l'information. L'attribution de l'importance se fait à

travers des poids, qui sont également appris par l'algorithme. Cela signifie simplement qu'il

apprend avec le temps quelle information est importante et laquelle ne l'est pas.

Dans un LSTM vous avez trois portes: entrée, oublier et sortie porte. Ces portes déterminent

s'il faut ou non laisser entrer une nouvelle entrée (porte d'entrée), supprimer l'information car

elle n'est pas importante (oublier la porte) ou la laisser influencer la sortie au pas de temps

courant (porte de sortie). Vous pouvez voir une illustration d'un RNN avec ses trois portes ci-

dessous:


IA & DL ENSA Khouribga

13

## Figure 7: LSTM

Les portes d'un LSTM sont analogiques, sous la forme de sigmoïdes, ce qui signifie qu'elles

vont de 0 à 1. Le fait qu'elles soient analogiques leur permet de faire une rétropropagation avec

elles.

Les problèmes posés par la disparition des gradients sont résolus grâce à LSTM, car les gra-

dients sont assez raides et l'entraînement est relativement court et la précision élevée.


## Partie de réalisation

Dans cette partie nous allons parler sur la méthode que nous avons adopté pour réaliser ce pro-

jet. Au premier lieu nous allons expliquer la base de données « dataset », après nous allons

expliquer le modèle utilisé pour classifier les différents tweets.

### Base de données ‘Dataset’ utilisée

Nous avons utilisé une base de données trouvée dans le site web Kaggle. Cette base de donnée

contient 2 champs : le premier est celui de tweets et le deuxième contient les différentes classes.

Il faut bien noter que cette base contient trois classes : Normal, abusive et hate. Normal signifie

que le tweet ne contient pas des mots agressifs, abusive est le contraire de normal, il contient

des mots injurieux, et hate signifie que le tweet est toxique.

Avant d’utiliser les données dans le modèle d’apprentissage profond, il faut tout d’abord net-

toyer ces données et les coder pour être, soit disant, des données correctes, alors avant tout,

nous allons supprimer les ponctuations, ainsi les Stop words, après nous passons les données

dans un Pipline qui consiste à travailler avec le processus de traitement de langage naturel, cela

vaut dire, nous devons faire la Tokenization : une sorte d’isoler les différents mots du tweet.

Stemming : le fait de convertir le mot à son origine par exemple le mot ‘fait’ après le stemming

devient ‘faire’. La dernière étape est la Vectorization : convertir tous les mots de tweets à une

matrice qui signifie que ce mot existe ou non dans le tweet. Le tableau suivant résume ce qui

disait au-dessus :

Prenons les trois tweets suivants :

1. Ensa est une école d’ingénieur
2. Ensa et Est sont deux écoles. Fst est une faculté


IA & DL ENSA Khouribga

15

Appliquons la 1er règle : suppression de ponctuation, stop words

1. Ensa est une école ingénieur
2. Ensa Est sont deux écoles Fst est faculté

Le processus de NLP :

Première phrase Deuxième phrase

Tokenization Ensa | est | école | ingé-

nieur

Ensa | Est | sont | deux

| école | Fst | est | fa-

culté

Stemming Ensa | être | école | in-

génieur

Ensa | Est | être | deux |

école | Fst | être | fa-

culté

La dernière étape dans le processus de NLP est la Vectorization alors dans notre exemple sera

comme suit :

Ensa Etre Ecole Ingénieur Est Deux Fst Faculté

1 1 1 1 1 0 0 0 0

2 1 1 1 0 1 1 1 1


Il faut aussi noter que nous devons coder les différents labels de base de donnée : hate = 1,

abusive = 0 et normal = 2

### Le modèle de Deep learning

Nous avons utilisé le modèle LSTM, ce modèle est dédié au traitement de langage. Alors pour

le construire, nous avons utilisé Tensorflow dans un environnement de python. Notre modèle

contient 3 couches : la première qui s’appelle une couche d’entrée, elle transforme les entiers

positifs (indices) en vecteurs Denses de taille fixe, la deuxième couche est LSTM de 100 unité

- à ajuster manuellement : par expérience-. La dernière couche est une couche de type Dense de

taille 3 (nombre de classes qu’on a).

Il faut bien noter que nous avons entrainé notre sous un environnement qui contient TPU (ce

n’ai pas dans un environnement CPU) cela qui nous a aidé à minimisé le temps d’entrainement

de notre modèle. Nous avons obtenu comme taux de précision : 62.99%.
